const { expect } = require("@playwright/test");
exports.WarenkorbPage = class WarenkorbPage {
    constructor(page) {
        this.productItems = page.locator('div.cartEntry');
        this.productNames = page.locator('a.cartEntry__articleLink');
        this.productPriceDiscount = page.locator('.priceNew--cart-discount');
        this.productPriceSingle = page.locator('.priceNew--cart-single');
        this.warenwertValue = page.locator('.cartOverview__summaryContainer--desktop div.summaryBox__line:has-text("Warenwert") .articlePrice');
    }

    /**
     * Get the product name
     * @param i
     * @returns {Promise<string>}
     */
    async getProductName(i) {
        const productNameText = await this.productNames.nth(i).innerText();
        return productNameText.toString().replace(/  +/g, ' ');
    }

    /**
     * Get the product price
     * @param i
     * @returns {Promise<string>}
     */
    async getProductPrice(i) {
        const productNew = await this.productItems.nth(i);
        let productPriceTextNew;
        let productPrice;
        if(await productNew.locator(this.productPriceSingle).isVisible()){
            productPriceTextNew = await productNew.locator(this.productPriceSingle).innerText();
            productPrice = productPriceTextNew.toString().replace(/[\n\sA-Za-z]/g, '');
        }
        else{
            const productPriceTextNewDiscount = await productNew.locator(this.productPriceDiscount).innerText();
            productPrice = productPriceTextNewDiscount.toString().replace(/\s/g, '');
        }
        return productPrice;
    }

    /**
     * Verify the products present in the cart
     * @param productList
     * @returns {Promise<void>}
     */
    async verifyProductsPresent(productList) {
        const count = productList.length;
        for (let i = 0; i < count; i++) {
            const productName = await this.getProductName(i);
            const productPrice = await this.getProductPrice(i);
            const productInList = productList.find(({name, price}) => name === productName && price === productPrice);
            expect(productInList).toBeDefined();
        }
    }

    /**
    * Verify the merchandise value for all products in the cart
    * @param productList
     */
    async verifyMerchandiseValue(productList) {
        let total = 0;
        // Extract the price of each product and add it to the total
        productList.forEach(product => {
            total += parseFloat(product.price.match(/.*?(?=€)/)[0].replace(/\./g, ''));
        });
        const warenwertValueText = await this.warenwertValue.innerText();
        const warenwertValue = warenwertValueText.toString().replace(/[\n\sA-Za-z\.]/g, '');
        expect(total).toBe(parseFloat(warenwertValue));
    }
}