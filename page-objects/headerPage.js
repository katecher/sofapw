exports.HeaderPage = class HeaderPage {
    constructor(page) {
        this.page = page;
        this.wishlistIcon = page.locator(".headerElement--wishlist a");
        this.meinKontoButton = page.locator('.headerElement--login a');
    }
    /**
     * Go to wishlist page
     * @returns {Promise<void>}
     */
    async clickWishlistIcon() {
        await this.wishlistIcon.click();
    }

    /**
     * Click on the "Mein Konto" button
     * @returns {Promise<void>}
     */
    async clickMeinKontoButton() {
        await this.meinKontoButton.click();
    }
}