const { expect } = require("@playwright/test");

exports.HomePage = class HomePage {
    constructor(page) {
        this.page = page;
        this.acceptCookies = page.getByText('Alle auswählen & bestätigen');
        this.statusLoginSpan = page.locator('.headerElement__status--login');
        this.productCategory = page.locator('.headerImageLinkPanel__container a span');
    }
    /**
     * Navigate to the home page
     * @returns {Promise<void>}
     */
    async goto() {
        await this.page.goto('/');
    }

    /**
     * Accept cookies
     * @returns {Promise<void>}
     */
    async clickAcceptCookies() {
        await this.acceptCookies.click();
    }

    /**
     * Verify that header element contains the user's name after login
     * @param user
     * @returns {Promise<void>}
     */
    async verifyStatusLogin(user) {
        await expect(await this.statusLoginSpan).toContainText(user.firstName);
        await expect(await this.statusLoginSpan).toContainText(user.lastName);
    }

    /**
     * Click on a random category
     * @returns {Promise<void>}
     */
    async clickOnRandomCategory() {
        const count = await this.productCategory.count();
        //TODO: investigate why this button is not clickable sometimes
         try{
             await this.productCategory.nth(Math.floor(Math.random() * count)).click();
             await this.page.waitForURL('**/*kategorieeinstieg*', {timeout: 3000});
         } catch (e) {
             await this.productCategory.nth(Math.floor(Math.random() * count)).click();
             await this.page.waitForURL('**/*kategorieeinstieg*');
         }
    }
}