const { expect } = require('@playwright/test');

exports.LoginPage = class LoginPage {
    constructor(page) {
        this.page = page;
        this.jetztRegistrierenButton = page.getByTestId('registerAccount');
        this.anmeldenButton = page.getByTestId('login-submit');
        this.emailAdresseField = page.getByTestId('loginEmailInput');
        this.passwordField = page.getByTestId('loginPasswordInput');
        this.loginErrorMessage = page.locator('#loginEmail-error');
    }

    /**
     * Click the "Jetzt Registrieren" button
     */
    async clickJetztRegistrierenButton() {
        //TODO: investigate why this button is not clickable sometimes
        try{
            await this.jetztRegistrierenButton.click();
            await this.page.waitForURL('**/registrierung', {timeout: 3000});
        } catch (e) {
            await this.jetztRegistrierenButton.click();
            await this.page.waitForURL('**/registrierung');
        }
    }

    /**
     * Click the "Anmelden" button
     */
    async clickAnmeldenButton() {
        await this.anmeldenButton.click();
    }

    /**
     * Fill the Login form
     * @param email
     * @param password
     */
    async fillAnmeldenForm(email, password) {
        await this.emailAdresseField.fill(email);
        await this.passwordField.fill(password);
        //TODO: Investigate why nothing happens after clicking Anmelden button
        // probably it can be relate to passwordField verification on frontend side
        await this.page.waitForTimeout(1000);
    }

    /**
     * Verify the login error message is displayed
     */
    async verifyLoginError() {
        await expect(await this.loginErrorMessage).toContainText('Benutzername nicht gefunden oder Passwort falsch');
    }
}