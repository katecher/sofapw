const {expect} = require("@playwright/test");
exports.KundenKontoPage = class KundenKontoPage {
    constructor(page) {
        this.welcomeMwssage = page.locator('.titleHeadline');
    }

    /**
     * Verify the first name and last name are present on KundenKonto page
     * @param user
     * @returns {Promise<void>}
     */
    async verifyFirstNameAndLastNameArePresent(user) {
        await expect(await this.welcomeMwssage).toContainText(user.firstName);
        await expect(await this.welcomeMwssage).toContainText(user.lastName);
    }
}