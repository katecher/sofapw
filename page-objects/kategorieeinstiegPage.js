exports.KategorieeinstiegPage = class KategorieeinstiegPage {
    constructor(page) {
        this.allProducts = page.locator("[data-testid^=p-id]");
        this.productPrice = page.locator("[data-testid^=p-id] > [class*=\"-9\"] > [class*=\"-0\"]");
        this.productName = page.locator("[data-testid^=p-id] > div[class*=\"-3\"] ");
        this.productBrand = page.locator("[data-testid^=p-id] > div[class*=\"-2\"] ");
        this.addToWishlistButton = page.locator("[data-testid^=p-id] [data-testid=\"wishlistHeart\"]");
    }


    /**
     * Get the product details for the product at index i
     * @param products
     * @param i
     * @returns {Promise<{price: string, name: string, brand: string}>}
     */
    async getProductDetails(products, i) {
        const productNameText = await this.productName.nth(i).innerText();
        const productName = productNameText.toString().replace(/^ /, '');
        const productPriceText = await this.productPrice.nth(i).innerText();
        const productPrice = productPriceText.toString().replace(/[\n\sA-Za-z]/g, '');
        let productBrand = await this.productBrand.nth(i).innerText() ;
        if(productBrand.toString().length > 1){
            productBrand = productBrand + ' ';
        }
        else{
            productBrand = '';
        }
        return {
            name: productBrand + productName,
            price: productPrice,
            brand: productBrand
        };
    }

    /**
     * Click the "Add to Wishlist" button for the product at index i
     * @param i
     */
    async clickAddToWishlistButton(i) {
        await this.addToWishlistButton.nth(i).click();
    }

    /**
     * Get 5 random products from the category
     * Click the "Add to Wishlist" button for each product
     * @returns {Promise<{name: string, price: string, brand: string}[]>}
     */
    async getFiveRandomProducts() {
        const products = await this.allProducts;
        const count = await products.count();
        let randomProducts = [];
        for (let i = 0; i < 5; i++) {
            const randomIndex = Math.floor(Math.random() * count);
            randomProducts.push(await this.getProductDetails(products, randomIndex));
            await this.clickAddToWishlistButton(randomIndex);
        }
        return randomProducts;
    }
}