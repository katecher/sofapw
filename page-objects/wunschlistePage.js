const { expect } = require("@playwright/test");
exports.WunschlistePage = class WunschlistePage {
    constructor(page) {
        this.page = page;
        this.productName = page.locator('.articleFullName__wishlistEntry');
        this.productPriceSingle = page.locator('.articlePricesNew__price--single');
        this.productPriceOriginal = page.locator('.articlePricesNew__price--original');
        this.productPriceDiscount = page.locator('.articlePricesNew__price--discount');
        this.productPriceSaving = page.locator('.priceNew--saving');
        this.productEntry = page.locator('.wishlist__articleList .wishlistEntry');
        this.zipCodeInput = page.locator('.wishlist__postalCodeArea [data-testid = "zipcode-logistic-inputInput"]');
        this.addAllProductsToCartButton = page.getByTestId('addAddToWishlist');
        this.addToCartButtons = page.locator('button:has-text("In den Warenkorb")');
        this.zumWarenkorbButton = page.locator('#overlayRight button:has-text("Zum Warenkorb")');
        this.allProductsWereAddedMessage = page.locator('#overlayRight div.addToCartOverlay__headerConfirm:has-text("Ihr Artikel befindet sich nun im Warenkorb.")');
    }

    /**
     * Enter the zip code in the input field
     * @param {string} zipCode - The zip code to be entered
     */
    async enterZipCode(zipCode) {
        await this.zipCodeInput.fill(zipCode);
    }

    /**
     * Click the "In den Warenkorb" button and wait for the page to navigate to the cart page
     */
    async clickZumWarenkorbButton() {
        await this.zumWarenkorbButton.click();
        await this.page.waitForURL('**/warenkorb');
    }

    /**
     * Click the "Alle Artikel in den Warenkorb" button and wait for the message to be displayed
     * @returns {Promise<void>}
     */
    async clickAddAllProductsToCartButton() {
        //TODO: investigate why the page is not loading properly
        await this.page.waitForTimeout(3000);
        await this.addAllProductsToCartButton.click();
        await expect(await this.allProductsWereAddedMessage).toBeVisible();
    }

    /**
     * Verify that Add to Cart buttons are visible
     * @returns {Promise<void>}
     */
    async verifyDeliveryInfoUpdated() {
        await expect(await this.addToCartButtons).toBeVisible();
    }

    /**
     * Get name of the product
     * @param {Locator} product - The product locator
     * @returns {Promise<string>}
     */
    async getProductName(product) {
        const productNameText = await product.locator(this.productName).innerText();
        return productNameText.toString().replace(/  +/g, ' ');
    }

    /**
     * Get price of the product
     * @param {Locator} product - The product locator
     * @returns {Promise<string>}
     */
    async getProductPrice(product) {
        let productPriceTextNew;
        let productPrice;
        if(await product.locator(this.productPriceSingle).isVisible()){
            productPriceTextNew = await product.locator(this.productPriceSingle).innerText();
            productPrice = productPriceTextNew.toString().replace(/[\n\sA-Za-z]/g, '');
        }
        else{
            const productPriceTextNewDiscount = await product.locator(this.productPriceDiscount).innerText();
            const productPriceTextNewOriginal = await product.locator(this.productPriceOriginal).innerText();
            const productPriceTextNewSaving = await product.locator(this.productPriceSaving).innerText();
            productPriceTextNew = productPriceTextNewDiscount.toString() + productPriceTextNewOriginal.toString() + productPriceTextNewSaving.toString();
            productPrice = productPriceTextNew.toString().replace(/[\n\sA-Za-z]/g, '')
        }
        return productPrice;
    }

    /**
     * Verify the products in the wishlist
     * @param {Array} productList - The list of products to be verified
     */
    async verifyProducts(productList) {
        const products = await this.productName;
        const count = await products.count();
        for (let i = 0; i < count; i++) {
            const product = await this.productEntry.nth(i);
            const productName = await this.getProductName(product);
            const productPrice = await this.getProductPrice(product);
            const receivedArrayMapped = productList.map(({name, price}) => ({name, price}));
            expect(receivedArrayMapped).toContainEqual({ name: productName, price: productPrice });
        }
    }
}