exports.RegistrierungPage = class RegistrierungPage {
    constructor(page) {
        this.page = page;
        this.anredeDropdown = page.getByTestId('salutation');
        this.vornameInput = page.getByTestId('firstNameInput');
        this.nachnameInput = page.getByTestId('lastNameInput');
        this.emailInput = page.locator('.accountNew [data-testid="emailInput"]');
        this.passwortInput = page.getByTestId('passwordInput');
        this.passwortWiederholenInput = page.getByTestId('password2Input');
        this.agbCheckbox = page.getByTestId('agbCheckbox');
        this.weiterButton = page.getByTestId('register-submit');
    }

    /**
     * Fill the registration form
     * @param user
     * @returns {Promise<void>}
     */
    async fillForm(user) {
        await this.anredeDropdown.selectOption( `${user.salutation}` );
        await this.vornameInput.fill(user.firstName);
        await this.nachnameInput.fill(user.lastName);
        await this.emailInput.fill(user.email);
        await this.passwortInput.fill(user.password);
        await this.passwortWiederholenInput.fill(user.password);
        await this.agbCheckbox.click({force: true});
    }

    /**
     * Click the "Weiter" button
     */
    async clickWeiterButtonAndVeryfyThatUrlChanged() {
        try {
            await this.weiterButton.click();
            await this.page.waitForURL('https://www.sofa.de');
        } catch (e) {
            await this.weiterButton.click();
            await this.page.waitForURL('https://www.sofa.de');
        }
    }
}