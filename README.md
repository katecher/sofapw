# 1. Manual Test Cases

### Document with manual test cases you can find [here](https://docs.google.com/spreadsheets/d/1g_mjAjcNZQR8Cwb1AYmYvkcrPuTeiPGvupSY_qsIqIE/edit?usp=sharing)

# 2. Automation Task: Sofade Playwright UI Tests And API tests for Petstore

## Getting Started 

### 1. Clone the repository

```
git@gitlab.com:katecher/sofapw.git
```
or
```
https://gitlab.com/katecher/sofapw.git
```

## How to run tests in docker container?

### 1. Build docker image

```docker build -t playwright-tests .```

### 2. Run API tests 

```docker run playwright-tests npm run api-test```

### 3. Run UI tests

```docker run -e PASSWORD=Passwrd playwright-tests npm run ui-test```

Set password to any value you want to use for new users.

## Test on gitlab CI

You can find test results on gitlab CI [here](https://gitlab.com/katecher/sofapw/-/pipelines)

# 4 Database Query challenge 

### Find repo with database query challenge [here](https://gitlab.com/katecher/sellerdb)

# 6. Performance test

### Find documentation with performance test [here](https://docs.google.com/document/d/1Saq8y-JLxgZ25-5ImUY-UInqJcOuohXiO-IZYBZy-fM/edit?usp=sharing)