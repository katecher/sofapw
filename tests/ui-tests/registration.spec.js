const { test, expect } = require('@playwright/test');
const { User } = require('../../helpers/ui/user');
const { LoginPage } = require('../../page-objects/loginPage');
const { RegistrierungPage } = require('../../page-objects/registrierungPage');
const {HomePage} = require('../../page-objects/homePage');
const {KundenKontoPage} = require('../../page-objects/kundenKontoPage');
const {KategorieeinstiegPage} = require('../../page-objects/kategorieeinstiegPage');
const {HeaderPage} = require('../../page-objects/headerPage');
const {WunschlistePage} = require('../../page-objects/wunschlistePage');
const {WarenkorbPage} = require('../../page-objects/warenkorbPage');

test.describe('Functional UI tests', () => {
    let user;
    let homePage;
    let loginPage;
    let registrierungPage;
    let kundenKontoPage;
    let kategorieeinstiegPage;
    let headerPage;
    let wunschlistePage;
    let warenkorbPage;

    test.beforeAll(async () => {
        user = new User();
    });

    test.beforeEach(async ({page}) => {
        homePage = new HomePage(page);
        loginPage = new LoginPage(page);
        registrierungPage = new RegistrierungPage(page);
        kundenKontoPage = new KundenKontoPage(page);
        kategorieeinstiegPage = new KategorieeinstiegPage(page);
        headerPage = new HeaderPage(page);
        wunschlistePage = new WunschlistePage(page);
        warenkorbPage = new WarenkorbPage(page);

        await homePage.goto();
        await homePage.clickAcceptCookies();
        await headerPage.clickMeinKontoButton();
    });

    test('New user registration', async ({page}) => {
            await test.step('Click Jetzt Registrieren button', async () => {
                await loginPage.clickJetztRegistrierenButton();
            });

            await test.step('Fill all required fields', async () => {
                await registrierungPage.fillForm(user);
                await registrierungPage.clickWeiterButtonAndVeryfyThatUrlChanged();
            });

            await test.step('Verify user is successfully registered', async () => {
                await homePage.verifyStatusLogin(user);
                await headerPage.clickMeinKontoButton();
                await page.waitForURL('**/kundenkonto');
                await kundenKontoPage.verifyFirstNameAndLastNameArePresent(user);
            });
        }
    );

    test('Login Scenario - successful login', async ({page}) => {
        await test.step('Login user', async () => {
            await loginPage.fillAnmeldenForm(user.email, user.password);
            await loginPage.clickAnmeldenButton();
        });

        await test.step('Verify user is successfully logged in', async () => {
            await homePage.verifyStatusLogin(user);
            await headerPage.clickMeinKontoButton();
            await page.waitForURL('**/kundenkonto');
            await kundenKontoPage.verifyFirstNameAndLastNameArePresent(user);
        });
    });

    test('Login Scenario - unsuccessful login', async ({page}) => {
        await test.step('Login user with invalid credentials', async () => {
            const wrongPassword = 'wrongpassword';
            await loginPage.fillAnmeldenForm(user.email, wrongPassword);
            await loginPage.clickAnmeldenButton();
        });

        await test.step('Verify user is not logged in', async () => {
            await loginPage.verifyLoginError();
        });
    });

    test('1x Shopping Scenario', async ({page}) => {
        let productList = [];
        //TODO: Login with random registered user.
        await test.step('Login user', async () => {
            await loginPage.fillAnmeldenForm(user.email, user.password);
            await loginPage.clickAnmeldenButton();
            await homePage.verifyStatusLogin(user);
        });

        await test.step('Add product to cart and proceed to checkout', async () => {
            await homePage.clickOnRandomCategory();
            productList =  await kategorieeinstiegPage.getFiveRandomProducts();
        });

        await test.step('Go to Wishlist', async () => {
            await headerPage.clickWishlistIcon();
            await page.waitForURL('**/wunschliste');
        });

        await test.step('Verify product details on Wishlist page', async () => {
            await wunschlistePage.verifyProducts(productList);

        });
        await test.step('Type Zip code', async () => {
            await wunschlistePage.enterZipCode('10719');
            await wunschlistePage.verifyDeliveryInfoUpdated();
        });

        await test.step('Add all products to the cart', async () => {
            await wunschlistePage.clickAddAllProductsToCartButton();
        });

        await test.step('Click Zum Warenkorb button', async () => {
            await wunschlistePage.clickZumWarenkorbButton();
        });

        await test.step('Verify that all products were added to the cart', async () => {
            await warenkorbPage.verifyProductsPresent(productList);
        });
        await test.step('Verify that merchandise value is correct', async () => {
            await warenkorbPage.verifyMerchandiseValue(productList);
        });
    });
});