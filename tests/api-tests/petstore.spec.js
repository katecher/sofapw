const { test, expect } = require('@playwright/test');
import { fakerDE as faker } from '@faker-js/faker'
import { Pet } from '../../helpers/api/pet.js';

const baseAPIURL = 'https://petstore.swagger.io';
test.describe('Petstore API tests', () => {
    const endpoint = '/v2/pet';
    const url = `${baseAPIURL}${endpoint}`;
    test('CREATE new pet ', async ({ request }) => {
        let response;
        const pet = new Pet();

        await test.step('Create new pet', async () => {
            response = await request.post(url, {data: pet.data});
        });

        await test.step('Verify response', async () => {
            expect(response.ok()).toBeTruthy();
            expect(response.status()).toBe(200);

            const resp = await response.json();
            expect(resp).toEqual(pet.data);
        });
    });

    test('CREATE new pet where name has special characters', async ({ request }) => {
        let response;
        const pet = new Pet().setName('.-()&?/#$%*-').build();

        await test.step('Create new pet', async () => {
            response = await request.post(url, {data: pet.data});
        });

        await test.step('Verify response', async () => {
            expect(response.ok()).toBeTruthy();
            expect(response.status()).toBe(200);

            const resp = await response.json();
            expect(resp).toEqual(pet.data);
        });
    });

    test('CREATE new pet where status is wrong type', async ({ request }) => {
        let response;
        const statusNumber = faker.number.int( {min: 1, max: 100} );
        const pet = new Pet().setStatus(statusNumber).build();

        await test.step('Create new pet where status has number type', async () => {
            response = await request.post(url, {data: pet.data});
        });

        await test.step('Verify response code is 200', async () => {
            expect(response.ok()).toBeTruthy();
            expect(response.status()).toBe(200);
        });

        await test.step('Verify response code has status as string value', async () => {
            pet.data.status = statusNumber.toString();
            const resp = await response.json();
            expect(resp).toEqual(pet.data);
        });
    });

    test('CREATE new pet with empty body', async ({ request }) => {
        let response;

        await test.step('Create new pet with empty body', async () => {
            response = await request.post(url, {data: {}});
        });

        await test.step('Verify response code is 200', async () => {
            expect(response.status()).toBe(200);
        });

        await test.step('Verify that id was created', async () => {
            const resp = await response.json();
            expect(resp.id).toBeDefined();
            expect(typeof resp.id).toBe('number');
        });
    });

    //TODO: Confirm requirements for this testcase, made assumption that 405 status code is expected
    test('CREATE a pet if ID has string value', async ({ request }) => {
        let response;
        const idString = faker.lorem.word();
        const pet = new Pet().setId(idString).build();

        await test.step('Create new pet if ID has string value', async () => {
            response = await request.post(url, {data: pet.data});
        });

        await test.step('Verify response code is 405', async () => {
            expect(response.status()).toBe(405);
        });
    });

    test('GET created pet by ID', async ({ request }) => {
        let response;
        let getResponse;
        const pet = new Pet();

        await test.step('Create new pet', async () => {
            response = await request.post(url, {data: pet.data});
            expect(response.status()).toBe(200);
        });

        await test.step('Get created pet by Id', async () => {
            getResponse = await request.get(`${url}/${pet.data.id}`);
            expect(getResponse.ok()).toBeTruthy();
            expect(getResponse.status()).toBe(200);
            const resp = await getResponse.json();
            expect(resp).toEqual(pet.data);
         });
    });

    test('GET pet by ID that doesnt exist', async ({ request }) => {
        let response;
        let getResponse;
        const pet = new Pet();

        await test.step('Create new pet', async () => {
            response = await request.post(url, {data: pet.data});
            expect(response.status()).toBe(200);
        });

        await test.step('Delete created pet by Id', async () => {
            getResponse = await request.delete(`${url}/${pet.data.id}`);
            expect(getResponse.status()).toBe(200);
        });

        await test.step('Get deleted pet by Id', async () => {
            getResponse = await request.get(`${url}/${pet.data.id}`);
        });

        await test.step('Verify response code is 200', async () => {
            expect(getResponse.status()).toBe(404);
        });

        await test.step('Verify response massage', async () => {
            expect(getResponse.statusText()).toBe('Not Found');
        });
    });

    //TODO: Confirm requirements for this testcase, if response code 400 0r 404 is expected
    test('GET pet by invalid Id as string', async ({ request }) => {
        let getResponse;
        const IdString = faker.lorem.words();

        await test.step('Get pet by wrong Id', async () => {
            getResponse = await request.get(`${url}/${IdString}`);
        });
        await test.step('Verify response code is 404', async () => {
            expect(getResponse.status()).toBe(400);
        });
        await test.step('Verify response massage', async () => {
            expect(getResponse.statusText()).toBe('Invalid ID supplied');
        });
    });

    //TODO: Confirm requirements for this testcase, if response code 400 0r 404 is expected
    test('GET pet by invalid Id with special characters', async ({ request }) => {
        let getResponse;
        const wrongId = '.-()&?/#$%*-g';

        await test.step('Get pet by wrong Id', async () => {
            getResponse = await request.get(`${url}/${wrongId}`);
        });
        await test.step('Verify response code is 404', async () => {
            expect(getResponse.status()).toBe(400);
        });
        await test.step('Verify response massage', async () => {
            expect(getResponse.statusText()).toBe('Invalid ID supplied');
        });
    });
});
