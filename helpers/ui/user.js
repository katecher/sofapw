import { fakerDE as faker } from '@faker-js/faker'
const dotenv = require('dotenv');

dotenv.config({
    path: '.env'
});

exports.User = class User {
    constructor() {
        this.salutation = faker.helpers.arrayElement(['Herr','Frau', 'Keine Angabe']);
        this.firstName = faker.person.firstName();
        this.lastName = faker.person.lastName();
        this.email = faker.internet.email({firstName: this.firstName, lastName: this.lastName});
        this.password = process.env.PASSWORD;
    }
}