import { fakerDE as faker } from '@faker-js/faker'
exports.Pet = class Pet {
    constructor() {
        this.data = {
            id: faker.number.int(),
            category: {
                id: faker.number.int(),
                name: faker.lorem.word(),
            },
            name: faker.person.firstName(),
            photoUrls: [],
            tags: [{id: faker.number.int(), name: "tagName_" + faker.lorem.word()}]
        }
    }

    setId(id) {
        this.data.id = id;
        return this;
    }

    setCategory(category) {
        this.data.category = category;
        return this;
    }

    setName(name) {
        this.data.name = name;
        return this;
    }

    setStatus(status) {
        this.data.status = status;
        return this;
    }

    build() {
        return this;
    }
}